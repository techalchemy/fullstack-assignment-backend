# news-weather-api
This api fetches news and weather data.
Also, it has added support for user login and signup using a JSON file.

## prerequisite
- node(v10 or above)

## setup
- go to the root directory
- run ```npm i```

## how to run
- for dev environment
    - ```npm run start```

